var path = require('path');
var aws = require('aws-sdk');
var q = require('q');
var pr = require(path.normalize(__dirname+'/../util/propertyReader'));
aws.config.loadFromPath(__dirname + '/mq-config.json');

// Instantiate SQS.
var sqs = new aws.SQS();

module.exports = {

    publishNotification: function (notification) {
        var deferred = q.defer();
        console.log('publishing notificcation');

        var params = {
            MessageBody: JSON.stringify(notification),
            QueueUrl: pr.properties.get("sqs.queueUrl"),
            DelaySeconds: 0
        };

        sqs.sendMessage(params, function (err, data) {
            if (err) {
                console.log(err);
                deferred.reject(err);;
            }
            else {
                deferred.resolve("Message Published Successfully");
            }
        });
        return deferred.promise;
    }

}

