var path = require('path');
var PropertiesReader = require('properties-reader');

module.exports= {
    properties : PropertiesReader(path.normalize(__dirname + '/dev.properties'))
}