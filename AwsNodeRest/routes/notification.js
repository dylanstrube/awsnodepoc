var uuId = require('node-uuid');
var path = require('path');
var express = require('express');
var publishMsg = require(path.normalize(__dirname+'/../mq/publishMsg'));
var logger = require(path.normalize(__dirname+'/../logger/logger')).logger;

var router = express.Router();

router.post('/', function(req, res) {
    var startDate =  new Date().getTime();
    var uuid = uuId.v4();
    var type = req.body.type;
    var text = req.body.text;
    var users = req.body.users;
    logger.info("Message Received, uuid:", uuid);
    var notification = {
        'notificationType': type,
        'notificationTxt': text,
        'users': users
    };
    console.log('Received Notification: ', notification);
    notification.uuid = uuid;
    notification.startDate = startDate;
    var mqPublishPromise = publishMsg.publishNotification(notification);
    mqPublishPromise.then(function (success) {
        console.log(success);
        var endDate =  new Date().getTime();
        logger.info(uuid+",", endDate-startDate);
        logger.info("Message published to MQ, uuid:", uuid);
        res.sendStatus(200);
    }).catch(function (err) {
        console.log(err);
        res.sendStatus(500);
    });
});

module.exports = router;
