/**
 * Created by RED7830 on 5/4/2016.
 */
var path = require('path');
var dbConn = require(path.normalize(__dirname + '/dbconfig'));

var connection = dbConn.connection;

module.exports.fetchUserNotifications = function(notificationId, callback, msg){
    var query = connection.query('SELECT USER_ID, NOTIFICATION_TEXT as notification FROM USER_NOTIFICATIONS UN \
                        JOIN NOTIFICATION N \
                        ON N.ID = UN.NOTIFICATION_ID \
                        WHERE NOTIFICATION_ID = ?', notificationId, function (err, result){
            console.log("query : ", query.sql);
            if(err){
                console.error(err);
                return;
            }
        console.log(result);
        callback(result, msg);
    }
    );
};
