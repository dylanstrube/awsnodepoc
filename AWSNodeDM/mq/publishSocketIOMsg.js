var aws = require('aws-sdk');
var queueUrl = "https://sqs.us-west-2.amazonaws.com/094708966468/SocketIONotifications";

// Load your AWS credentials and try to instantiate the object.
aws.config.loadFromPath(__dirname + '/config.json');

// Instantiate SQS.
var sqs = new aws.SQS();

exports.publishSocketIOMsg = function(msg){

    console.log('publishing notificcation', msg);

    var params = {
        MessageBody: JSON.stringify(msg),
        QueueUrl: queueUrl,
        DelaySeconds: 0
    };

    sqs.sendMessage(params, function(err, data) {
        if(err) {
            console.log(err);
        }
        else {
            console.log('message published successfully');
        }
    });
};

