var express  = require('express');
var path = require('path');
var app      = express();
var aws      = require('aws-sdk');
var email    = require(path.normalize(__dirname + '/email/sendEmail'));
var msgLog    = require(path.normalize(__dirname + '/db/logging'));
var userNotification    = require(path.normalize(__dirname + '/db/UserNotifications.js'));

var s = "https://sqs.us-west-2.amazonaws.com/094708966468/EmailNotification";
var queueUrl = s;
var receipt  = "";

// Load your AWS credentials and try to instantiate the object.
aws.config.loadFromPath(__dirname + '/email/config.json');

// Instantiate SQS.0
var sqs = new aws.SQS();

var receiveMsg = function(){
    var params = {
        QueueUrl: queueUrl,
        VisibilityTimeout: 600 // 10 min wait time for anyone else to process.
    };

    sqs.receiveMessage(params, function(err, data) {
        if(err) {
            console.log(err);
            setTimeout(receiveMsg, 1000);
        } else {
            if ((data.Messages) && (typeof data.Messages[0] !== 'undefined' && typeof data.Messages[0].Body !== 'undefined')) {
                setTimeout(receiveMsg, 0);
                msgLog.msgLog(data.Messages[0].Body);
                var body = JSON.parse(data.Messages[0].Body);
                userNotification.fetchUserNotifications(body.notificationId, triggerEmail, data);
            } else {
                setTimeout(receiveMsg, 1000);
            }
        }
    });
};

receiveMsg();

var server = app.listen(8085, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('AWS SQS example app listening at http://%s:%s', host, port);
});


function createDeleteMessageCallback(data) {
    return function() {
        var obj = {
            "QueueUrl": queueUrl,
            "ReceiptHandle": data.Messages[0].ReceiptHandle
        };

        var func = function (err, data) {
            if (err) {
                console.log(err);
                return;
            }
            console.log("Message deleted");
        };

        sqs.deleteMessage(obj, func);
    };
}


var triggerEmail = function(userEmailNotifications, msg){
    for(var i= 0; i< userEmailNotifications.length;i++) {
        console.log(userEmailNotifications && userEmailNotifications[i] && userEmailNotifications[i].notification);
        if (userEmailNotifications && userEmailNotifications[i] && userEmailNotifications[i].notification) {
            var deleteMessageCallback = createDeleteMessageCallback(msg);
            email.sendEmail(userEmailNotifications[i].notification, deleteMessageCallback);
        }
    }
}
