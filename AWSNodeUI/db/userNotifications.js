/**
 * Created by RED7830 on 5/4/2016.
 */
var path = require('path');
var dbConn = require(path.normalize(__dirname + '/dbconfig'));
var utils = require(path.normalize(__dirname + '/../util'+'/common-functions'));

var connection = dbConn.connection;

module.exports.mapUserNotifications = function(notificationId, users){
    var totalUsers = users.length;
    if(users && utils.isArray(users)) {
        users.forEach(function (user) {
            setTimeout(prepUserNotification(user), 0);
        });
    }

    function prepUserNotification(user) {
        var userNotification = {
            NOTIFICATION_ID: notificationId,
            USER_ID: user,
            NOTIFICATION_TYP_ID: 1
        };
        setTimeout(insertUserNotification(userNotification), 0);
    }

    function insertUserNotification(userNotification) {
        var query = connection.query('INSERT INTO USER_NOTIFICATIONS SET ?', userNotification, function (err, result) {
                console.log("query : ", query.sql);
                if (err) {
                    console.error(err);
                    return;
                }
                console.log("User Notifications mapped successfully");
            }
        );
    };
};