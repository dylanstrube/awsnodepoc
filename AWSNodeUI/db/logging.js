var path = require('path');
var dbConn = require(path.normalize(__dirname + '/dbconfig'));

var connection = dbConn.connection;

module.exports.msgLog = function(msg){
    var dbMsg = {
        'MSG' : msg
    };
    var query = connection.query('INSERT INTO MSG_LOG SET ?', dbMsg, function (err, result){
            console.log("query : ", query.sql);
            if(err){
                console.error(err);
                return;
            }
        }
    );
};
