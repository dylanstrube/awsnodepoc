var app = require('express')();
var path = require('path');
var server = require('http').createServer(app);
var bodyParser = require('body-parser');
var dbConn = require(path.normalize(__dirname + '/db/dbconfig'));
var io = require('socket.io').listen(server);
var cors = require('cors');
var logger = require(path.normalize(__dirname+'/logger/logger')).logger;
var aws = require('aws-sdk');

app.use(cors());

app.use(bodyParser.urlencoded({extended : false}));
app.use(bodyParser.json());

var connection = dbConn.connection;

connection.connect(function(err, result){
    if(err){
        console.log(err);
        return;
    }
    console.log('Connection is Successful');
});

app.get('/messages', function(req, res, next){

    var userId = req.query.recipient;
    var date = new Date();
    var sql = "SELECT NOTIFICATION_TYP_NME AS sender, NOTIFICATION_TEXT as body, USER_ID as User\ " +
        ", N.ID as id FROM NOTIFICATION N  JOIN USER_NOTIFICATIONS UN ON N.ID = UN.NOTIFICATION_ID JOIN NOTIFICATION_TYPE_VAL NTV ON NTV.ID = N.NOTIFICATION_TYP_ID";

    if(userId){
        sql += " WHERE USER_ID = '"+userId+"' AND NTV.ID = 2";
    }
    var query = connection.query(sql, function(err, result, fields){
        console.log(query.sql);
        if(err) {
            console.log(err);
            res.sendStatus(500);
        }
        res.json(result);
    });
});


app.get('/', function(req, res){
    res.sendFile(__dirname+'/2.html');
});

app.post('/messages/:id/', function(req, res, next) {
    res.sendStatus(200);
});

app.delete('/messages/:id/', function(req, res, next) {

    var userId = req.query.recipient
    var id = req.params.id;
    var query = connection.query("DELETE FROM USER_NOTIFICATIONS WHERE NOTIFICATION_ID = ?", id, function(err, result, fields){
        console.log(query.sql);
        if(err){
            console.log(err);
            res.sendStatus(500);
        }
        else{
            res.sendStatus(200);
        }
    });
});

server.listen(8082);

var s = "https://sqs.us-west-2.amazonaws.com/094708966468/SocketIONotifications";
var queueUrl = s;
var receipt  = "";

// Load your AWS credentials and try to instantiate the object.
aws.config.loadFromPath(__dirname + '/config.json');
var sqs = new aws.SQS();

io.sockets.on('connection', function(socket){
    var result = receiveMsg(socket);
    //socket.emit('message', result);
});

var receiveMsg = function (socket) {
    var params = {
        QueueUrl: queueUrl,
        VisibilityTimeout: 600 // 10 min wait time for anyone else to process.
    };

    sqs.receiveMessage(params, function(err, data) {
        if(err) {
            console.log(err);
            setTimeout(receiveMsg, 0);
        } else {
            if ((data.Messages) && (typeof data.Messages[0] !== 'undefined' && typeof data.Messages[0].Body !== 'undefined')) {
                setTimeout(receiveMsg, 0);
                var body = JSON.parse(data.Messages[0].Body);
                logger.info("Message Received: uuid:", body.uuid);
                var sql = "SELECT NOTIFICATION_TYP_NME AS sender, NOTIFICATION_TEXT as body, USER_ID as recipient , N.ID as id\ " +
                    "FROM NOTIFICATION N  JOIN USER_NOTIFICATIONS UN ON N.ID = UN.NOTIFICATION_ID\ " +
                    " JOIN NOTIFICATION_TYPE_VAL NTV ON NTV.ID = N.NOTIFICATION_TYP_ID WHERE N.ID = ?"
                var query = connection.query(sql, body.notificationId, function(err, result, fields){
                    console.log(query.sql);
                    if(err) {
                        console.log(err);
                        res.sendStatus(500);
                    }
                    io.sockets.emit('message', result);
                    deleteMessage(data);
                    console.log(result);
                });
            } else {
                setTimeout(receiveMsg, 1000);
            }
        }
    });
}

function deleteMessage(data) {
    var msg0 = data.Messages[0];
    var obj = {
            "QueueUrl": queueUrl,
            "ReceiptHandle": msg0.ReceiptHandle
        };

        var func = function (err, data) {
            if (err) {
                console.log(err);
                return;
            }
            console.log("Message deleted");
            var body = JSON.parse(msg0.Body);
            if(body.uuid) {
                var endDate = new Date().getTime();
                logger.info(body.uuid+",", endDate-Number(body.startDate));
                logger.info("Message Processed: uuid:", body.uuid);
            }
        };

        sqs.deleteMessage(obj, func);
}