var app = require('express')();
var path = require('path');
var q = require('q');
var http = require('http').Server(app);
var bodyParser = require('body-parser');
var dbConn = require(path.normalize(__dirname + '/db/dbconfig'));
var publishMesg = require('./mq/publishMsg');
var userNotification = require(path.normalize(__dirname + '/db/userNotifications'));
var mqListener = require(__dirname + '/listener/RestMsgMqListener');
var logger = require(path.normalize(__dirname+'/logger/logger')).logger;

app.use(bodyParser.urlencoded({extended : false}));
app.use(bodyParser.json());

app.get('/', function(req, res){
    var data = {
        'data' : ""

    }
    data.data = "Hello, Sri !! rest API";
    res.json(data);

});

var connection = dbConn.connection;

connection.connect(function(err, result){
    if(err){
        logger.error(err);
        return;
    }
    logger.info('Connection is Successful');
});

app.get('/create', function(req, res){

    var query = connection.query('SELECT MAX(ID) FROM NOTIFICATION;', function(err, result, fields){
        if(err) {
            logger.error(err);
            return;
        }
        console.log("result", result);
        res.json(result);
    });
});

app.get('/notification', function(req, res){

    var query = connection.query('SELECT * FROM NOTIFICATION', function(err, result, fields){
        if(err) {
            logger.error(err);
            return;
        }
        console.log("result", result);
        res.json(result);
    });
});


var processNotification = function (msg) {
    var deferred = q.defer();
    var uuid = msg.uuid;
    var notification = {
        'NOTIFICATION_TYP_ID': msg.notificationType,
        'NOTIFICATION_TEXT' : msg.notificationTxt
    };
    var query = connection.query('insert into NOTIFICATION set ? ;', notification, function (err, result){
        logger.silly("query : ", query.sql);
        if(err){
            logger.error(err);
            deferred.reject(err);
        }
        logger.debug("Inserted Notification in DB", uuid);
        var usrNotificationPromise = userNotification.mapUserNotifications(result.insertId, msg.users);

        usrNotificationPromise.then(function (succ) {
            logger.debug("Mapped Notification to users in DB", uuid);
            var queueUrl;

            if(msg.notificationType === 1){
                queueUrl = "https://sqs.us-west-2.amazonaws.com/094708966468/EmailNotification";
            }
            else{
                queueUrl = "https://sqs.us-west-2.amazonaws.com/094708966468/SocketIONotifications";
            }
            logger.debug("Message pushing to queue", uuid);
            publishMesg.publishNotification(result.insertId, msg.notificationTxt, queueUrl, uuid, msg.startDate).then(function (succMsg) {
                logger.debug("Message is published to queue",uuid);
                deferred.resolve('Notification Received');
            }).catch(function (err) {
                logger.error(err);
                deferred.reject(err);
            });
        }).catch(function (err) {
            logger.error(err);
        });
    });
    return deferred.promise;
};

//connection.end();
app.listen(8081);



var RestMsgNotificationListener = function () {
    var readMsg = mqListener.receiveMsg();
    var mqPromise = readMsg.call(mqListener);
    mqPromise.then(function (sqsMsg) {
        if ((sqsMsg.Messages) && (typeof sqsMsg.Messages[0] !== 'undefined' && typeof sqsMsg.Messages[0].Body !== 'undefined')) {
            setTimeout(RestMsgNotificationListener, 0);
            var body = JSON.parse(sqsMsg.Messages[0].Body);
            logger.info("Message Received: uuid:", body.uuid);
            processNotification(body).then(function (succMsg) {
                var msg0 = sqsMsg.Messages[0];
                var delMsgPromise = mqListener.deleteMessage(msg0);
                delMsgPromise.then(function (delSucc) {
                    if(body.uuid) {
                        var endDate = new Date().getTime();
                        logger.info(body.uuid+",", endDate-Number(body.startDate));
                        logger.info("Message Processed: uuid:", body.uuid);
                    }
                    console.log(delSucc);
                });
            });
        }
        else{
            setTimeout(RestMsgNotificationListener, 1000);
        }
    }).catch(function (err) {
        logger.error(err);
        setTimeout(RestMsgNotificationListener, 0);
    })
};

RestMsgNotificationListener();