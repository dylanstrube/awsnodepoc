var winston = require('winston');
var path = require('path');
var pr = require(path.normalize(__dirname+'/../util/propertyReader'));
module.exports.logger = new (winston.Logger)({
    transports: [
        new (winston.transports.File)({
            name: 'info-file',
            filename: path.join(pr.properties.get('log.file.path'), 'node-listener.log'),
            level: 'silly',
            json: false,
            timestamp: true
        }),
        new (winston.transports.File)({
            name: 'error-file',
            filename: 'node-rest-error.log',
            level: 'error',
            json: false,
            timestamp: true
        })
    ]
});