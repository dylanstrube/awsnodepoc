/**
 * Created by RED7830 on 5/4/2016.
 */
var q = require('q');
var path = require('path');
var dbConn = require(path.normalize(__dirname + '/dbconfig'));
var utils = require(path.normalize(__dirname + '/../util'+'/common-functions'));
var logger = require(path.normalize(__dirname+'/../logger/logger')).logger;

var connection = dbConn.connection;

exports.mapUserNotifications = function(notificationId, users){
    var deferred = q.defer();
    if(users && utils.isArray(users)) {
        users.forEach(function (user) {
            var userNotification = {
                NOTIFICATION_ID: notificationId,
                USER_ID: user
            };
            var query = connection.query('INSERT INTO USER_NOTIFICATIONS SET ?', userNotification, function (err, result) {
                logger.silly("query : ", query.sql);
                    if (err) {
                        logger.error(err);
                        deferred.reject(err);
                    }
                    deferred.resolve("User Notifications mapped successfully");
                }
            );
        });
    }
    return deferred.promise;
};