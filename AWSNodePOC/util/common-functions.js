/**
 * Created by RED7830 on 5/5/2016.
 */

module.exports.isArray = function(obj){
    if(obj && toString.call(obj)=== "[object Array]"){
        return true;
    }
    return false;
}