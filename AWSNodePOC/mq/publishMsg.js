var aws = require('aws-sdk');
var q = require('q');
//var queueUrl = "https://sqs.us-west-2.amazonaws.com/094708966468/PublishNotification";

// Load your AWS credentials and try to instantiate the object.
aws.config.loadFromPath(__dirname + '/config.json');

// Instantiate SQS.
var sqs = new aws.SQS();

exports.publishNotification = function(id, text, queueUrl, uuid, startDate){
    var deferred = q.defer();
    console.log('publishing notificcation', text);

    var body = {
        'notificationId' : id,
        'notificationText' : text,
        'uuid' : uuid,
        'startDate' : startDate
    };

    var params = {
        MessageBody: JSON.stringify(body),
        QueueUrl: queueUrl,
        DelaySeconds: 0
    };

    sqs.sendMessage(params, function(err, data) {
        if(err) {
            console.log(err);
            deferred.reject(err);
        }
        else {
            console.log('message published successfully');
            deferred.resolve('message published successfully');
        }
    });
    return deferred.promise;
};

