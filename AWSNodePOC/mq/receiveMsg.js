var express  = require('express');
var path = require('path');
var app      = express();
var aws      = require('aws-sdk');
var email    = require(path.normalize(__dirname + '/../email/sendEmail'));
var msgLog    = require(path.normalize(__dirname + '/../db/logging'));

var queueUrl = "https://sqs.us-west-2.amazonaws.com/094708966468/PublishNotification";
var receipt  = "";

// Load your AWS credentials and try to instantiate the object.
aws.config.loadFromPath(__dirname + '/config.json');

// Instantiate SQS.
var sqs = new aws.SQS();

var receiveMsg = function(){
    var params = {
        QueueUrl: queueUrl,
        VisibilityTimeout: 600 // 10 min wait time for anyone else to process.
    };

    sqs.receiveMessage(params, function(err, data) {
        if(err) {
            console.log(err);
        }
        else {
            if ((data.Messages) && (typeof data.Messages[0] !== 'undefined' && typeof data.Messages[0].Body !== 'undefined')) {
                msgLog.msgLog(data.Messages[0].Body);
                email.sendEmail(JSON.parse(data.Messages[0].Body).notificationText, deleteMsg(sqs, data));
        }
    }
});
};

var server = app.listen(8081, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('AWS SQS example app listening at http://%s:%s', host, port);
});


var deleteMsg = function(sqs, data){
    sqs.deleteMessage({
        "QueueUrl" : queueUrl,
        "ReceiptHandle" :data.Messages[0].ReceiptHandle
    }, function (err, data){
        if(err){
            console.log(err);
        }
        else{
            console.log("Message deleted");
        }
    });

};

setInterval(receiveMsg, 1000);
