var q = require('q');
var aws = require('aws-sdk');
var path = require('path');
var pr = require(path.normalize(__dirname+'/../util/propertyReader'));

aws.config.loadFromPath(__dirname + '/mq-config.json');
var sqs = new aws.SQS();

module.exports = {

    params : {
            QueueUrl: pr.properties.get("sqs.queueUrl"),
            VisibilityTimeout: 600 // 10 min wait time for anyone else to process.
    },

    receiveMsg : function() {
        return function () {
            var deferred = q.defer();
            sqs.receiveMessage(this.params, function (err, msg) {
               if (err) {
                   console.log(err);
                   deferred.reject(err);
               } else {
                   deferred.resolve(msg);
               };
            });
            return deferred.promise;
        };
    },

    deleteMessage : function (data) {
        var deferred = q.defer();
        if (data && data.ReceiptHandle) {
            console.log("Deleting Msg from mq");
            var obj = {
                "QueueUrl": this.params.QueueUrl,
                "ReceiptHandle": data.ReceiptHandle
            };
            var sqsDeleteCallback = function (err, data) {
                if (err) {
                    console.log(err);
                    deferred.reject(err);
                }
                deferred.resolve("Message deleted");
            };
            sqs.deleteMessage(obj, sqsDeleteCallback);
        }
        return deferred.promise;
    }

};
